*** Settings ***
Resource    Firebase.robot
Resource    Grafana.robot
Resource    Sheets.robot
Suite Setup       Open Chrome
Suite Teardown    Close Chrome
Test Teardown    Run Keyword If Test Failed    Log Variables

*** Keywords ***
Report9
    ${activeUser}    Get Active User
    Set Report    C    ${activeUser}
    Log To Console    SET activeUser ${activeUser} To C

Report12
    ${activeUser}    Get Active User
    Set Report    D    ${activeUser}
    Log To Console    SET activeUser ${activeUser} To D

Report18
    ${activeUser}    Get Active User
    Set Report    E    ${activeUser}
    Log To Console    SET activeUser ${activeUser} To E

Report22
    ${crashlytics}     Get Crashlytics
    ${activeUser}      Get Active User
    ${events}          Get Events
    Set Report    B    ${crashlytics}
    Log To Console    SET crashlytics ${crashlytics} To B
    Set Report    F    ${activeUser}
    Log To Console    SET activeUser ${activeUser} To F
    Set Report    G    ${events[0]}
    Log To Console    SET events ${events[0]} To G
    Set Report    H    ${events[1]}
    Log To Console    SET events ${events[1]} To H
    Set Report    I    ${events[2]}
    Log To Console    SET events ${events[2]} To I
    Set Report    J    ${events[3]}
    Log To Console    SET events ${events[3]} To J
    Set Report    K    ${events[4]}
    Log To Console    SET events ${events[4]} To K
    Set Report    L    ${events[5]}
    Log To Console    SET events ${events[5]} To L
    Set Report    M    ${events[6]}
    Log To Console    SET events ${events[6]} To M
    ReportGrafana
    
ReportGrafana
    Get Firebase Report Screen
    Get ADMD Screen
    Get Mobile-BE Screen
    Get System Monitor D01_1 Screen
    Get System Monitor D01_2 Screen
    
*** Test Cases ***
RunReport
    ${currentHour}  Get Time  hour
    ${currentHour}    Get Variable Value    ${setHour}    ${currentHour}
    ${currentHour}    Convert To Integer    ${currentHour}
    Log To Console    ${currentHour}
    @{time9}    Create List   ${08}    ${09}    ${10}
    @{time12}   Create List   ${11}    ${12}    ${13}
    @{time18}   Create List   ${17}    ${18}    ${19}
    @{time22}   Create List   ${21}    ${22}    ${23}
    Run Keyword If     ${currentHour} in ${time9}     Report9
    Run Keyword If     ${currentHour} in ${time12}    Report12
    Run Keyword If     ${currentHour} in ${time18}    Report18
    Run Keyword If     ${currentHour} in ${time22}    Report22

Reportnn
    #ReportGrafana
    Report22
    #Cap Firebase



#robot -d Result -t Reportnn report.robot